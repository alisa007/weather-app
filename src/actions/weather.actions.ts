import { Action } from '@ngrx/store';
import { TableRow } from '../interfaces/TableRow';
import { Forecast } from '../interfaces/Forecast';

export enum ActionTypes {
    GetWeather = '[Weather] Get Weather',
    GetWeatherSuccess = '[Weather] Get Weather Success',
    GetForecast = '[Weather] Get Forecast',
    GetForecastSuccess = '[Weather] Get Forecast Success',
    ShowForecast = '[Weather] Show forecast',
}

export class GetWeather implements Action {
    readonly type = ActionTypes.GetWeather;
}

export class GetWeatherSuccess implements Action {
    readonly type = ActionTypes.GetWeatherSuccess;
    constructor(public payload: TableRow[]) {}
}

export class GetForecast implements Action {
    readonly type = ActionTypes.GetForecast;
    constructor(public payload: number) {}
}

export class ShowForecast implements Action {
    readonly type = ActionTypes.ShowForecast;
    constructor(public payload: number) {}
}

export class GetForecastSuccess implements Action {
    readonly type = ActionTypes.GetForecastSuccess;
    constructor(public payload: { cityId: number, forecast: Forecast[] }) {}
}

export type Actions =
    | GetWeather
    | GetWeatherSuccess
    | GetForecast
    | ShowForecast
    | GetForecastSuccess;
