import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { CityEnum } from '../enums/city.enum';
import { Weather } from '../interfaces/Weather';

const appId = 'f8a64ba4ad70390bc25d6f7d3c277417';
const baseUrl = 'http://api.openweathermap.org/data/2.5/';
const units = 'metric';

@Injectable()
export class ApiService {
    constructor(private http: HttpClient) { }

    getWeather(cities: CityEnum[]): Observable<Weather[]> {
        return this.http.get(`${baseUrl}group?id=${cities.join(',')}&appid=${appId}&units=${units}`)
            .pipe(
                map((response: any) => response.list)
            );
    }

    getWeatherForecast(city: CityEnum): Observable<Weather[]> {
        return this.http.get(`${baseUrl}forecast?id=${city}&appid=${appId}&units=${units}`)
            .pipe(
                map((response: any) => response.list)
            );
    }
}
