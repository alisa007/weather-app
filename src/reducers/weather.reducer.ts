import { createSelector } from '@ngrx/store';
import { ActionTypes, Actions } from '../actions/weather.actions';
import { TableRow } from '../interfaces/TableRow';

export interface State {
    showForecast: number;
    cities: TableRow[];
}

export const initialState: State = {
    showForecast: 0,
    cities: [],
};

export function weatherReducer(state = initialState, action: Actions): State {
    switch (action.type) {
        case ActionTypes.GetWeatherSuccess:
            return {
                ...state,
                cities: action.payload,
            };
        case ActionTypes.GetForecastSuccess:
            return {
                ...state,
                showForecast: action.payload.cityId,
                cities: state.cities.map(city => {
                    if (city.id === action.payload.cityId) {
                        city.forecast = action.payload.forecast;
                    }

                    return city;
                })
            };
        case ActionTypes.ShowForecast:
            return {
                ...state,
                showForecast: action.payload,
            };
        default:
            return state;
    }
}

const selectFeature = (state: any): State => state.weather;
export const getCities = createSelector(selectFeature, (state: State): TableRow[] => state.cities);
export const getSelectedCity = createSelector(selectFeature, (state: State): TableRow => {
    return state.cities.find(city => city.id === state.showForecast);
});
