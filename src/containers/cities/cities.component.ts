import { AfterViewInit, Component } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { CityEnum } from '../../enums/city.enum';
import { TableRow } from '../../interfaces/TableRow';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { State } from '../../reducers/weather.reducer';
import { GetWeather, ShowForecast, GetForecast } from '../../actions/weather.actions';
import { getCities, getSelectedCity } from '../../reducers/weather.reducer';

@Component({
    selector: 'cities',
    templateUrl: './cities.component.html',
})
export class CitiesComponent implements AfterViewInit {
    cities$: Observable<TableRow[]>;
    city$: Observable<TableRow>;

    constructor(private apiService: ApiService, private store: Store<State>) {
        this.cities$ = store.pipe(select(getCities));
        this.city$ = store.pipe(select(getSelectedCity));
    }

    ngAfterViewInit() {
        this.store.dispatch(new GetWeather());
    }

    expandForecast(cityId: CityEnum): void {
        this.store.dispatch(new ShowForecast(cityId));
    }

    getForecast(cityId: CityEnum): void {
        this.store.dispatch(new GetForecast(cityId));
    }
}
