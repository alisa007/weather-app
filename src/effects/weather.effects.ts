import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CityEnum } from '../enums/city.enum';
import { ApiService } from '../services/api.service';
import { ActionTypes, GetForecast } from '../actions/weather.actions';

@Injectable()
export class WeatherEffects {
    @Effect()
    getForecast$: Observable<Action> = this.actions.pipe(
        ofType(ActionTypes.GetForecast),
        switchMap((action: GetForecast) =>
            this.apiService.getWeatherForecast(action.payload).pipe(
                map(cities => ({ type: ActionTypes.GetForecastSuccess, payload: {
                    cityId: action.payload,
                    forecast: cities.map(city => ({
                        temp: city.main.temp,
                        dt: city.dt_txt,
                    })),
                }}))
            )
        )
    );

    @Effect()
    GetWeather$: Observable<Action> = this.actions.pipe(
        ofType(ActionTypes.GetWeather),
        switchMap(() =>
            this.apiService.getWeather([
                CityEnum.London,
                CityEnum.Moscow,
                CityEnum.Kiev,
                CityEnum.Paris,
                CityEnum.Amsterdam,
            ]).pipe(
                map(cities => ({ type: ActionTypes.GetWeatherSuccess, payload: cities.map(city => ({
                        id: city.id,
                        name: city.name,
                        icon: `https://openweathermap.org/img/w/${city.weather[0].icon}.png`,
                        temp: city.main.temp,
                        wind: city.wind.speed,
                    }))
                }))
            ),
        )
    );

    constructor(private actions: Actions, private apiService: ApiService) {}
}
