export interface Weather {
    weather: Icon[];
    main: Temp;
    wind: Wind;
    dt: number;
    dt_txt: string;
    id?: number;
    name?: string;
}

interface Icon {
    id: number;
    main: string;
    description: string;
    icon: string;
}

interface Temp {
    temp: number;
    pressure: number;
    humidity: number;
    temp_min: number;
    temp_max: number;
}

interface Wind {
    speed: number;
    deg: number;
}
