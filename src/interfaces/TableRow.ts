import { Forecast } from './Forecast';

export interface TableRow {
    id: number;
    name: string;
    icon: string;
    temp: number;
    wind: number;
    forecast?: Forecast[];
}
