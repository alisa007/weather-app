export interface Forecast {
    dt: number;
    temp: number;
}
