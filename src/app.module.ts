import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule, MatTableModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { weatherReducer } from './reducers/weather.reducer';
import { EffectsModule } from '@ngrx/effects';
import { WeatherEffects } from './effects/weather.effects';

// Components
import { AppComponent } from './app.component';
import { WeatherMasterViewComponent } from './components/weather-master-view/weather-master-view.component';
import { WeatherDetailViewComponent } from './components/weather-detail-view/weather-detail-view.component';
import { LoaderComponent } from './components/loader/loader. component';
import { ApiService } from './services/api.service';

// Containers
import { CitiesComponent } from './containers/cities/cities.component';

const routes: Routes = [
    { path: 'weather', component: CitiesComponent },
    { path: '', redirectTo: '/weather', pathMatch: 'full' },
];

@NgModule({

    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatProgressSpinnerModule,
        MatTableModule,
        ChartsModule,
        StoreModule.forRoot({ weather: weatherReducer }),
        EffectsModule.forRoot([WeatherEffects]),
        RouterModule.forRoot(routes),
        StoreDevtoolsModule.instrument()
    ],
    declarations: [
        AppComponent,
        LoaderComponent,
        WeatherMasterViewComponent,
        CitiesComponent,
        WeatherDetailViewComponent,
    ],
    providers: [ApiService],
    bootstrap: [AppComponent]
})
export class AppModule {}
