import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TableRow } from '../../interfaces/TableRow';

@Component({
    selector: 'weather-master-view',
    templateUrl: './weather-master-view.component.html',
    styleUrls: ['./weather-master-view.component.less']
})
export class WeatherMasterViewComponent {
    @Input() cities: TableRow[];
    @Output() expandForecast = new EventEmitter<number>();
    @Output() getForecast = new EventEmitter<number>();

    cols = ['icon', 'name', 'temp', 'wind'];

    showForecast(id: number, hasForecast: boolean): void {
        if (hasForecast) {
            this.expandForecast.emit(id);
        } else {
            this.getForecast.emit(id);
        }
    }
}
