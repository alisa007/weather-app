import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TableRow } from '../../interfaces/TableRow';

@Component({
    selector: 'weather-detail-view',
    templateUrl: './weather-detail-view.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./weather-detail-view.component.less']
})
export class WeatherDetailViewComponent {
    @Input() model: TableRow;
    @Input() size: number;

    options = {
        responsive: true,
        scales : {
            xAxes : [ {
                gridLines : {
                    display : false
                }
            } ]
        },
        legend: {
            display: false,
        },
    };

    get data() {
        return [{
            data: this.model.forecast.map(f => f.temp),
            fill: false
        }];
    }

    get labels() {
        return this.model.forecast
            .slice(0, this.size)
            .map(f => f.dt.toString().match(/[0-9]{2}:[0-9]{2}/));
    }
}
